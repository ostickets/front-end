# Instruções para execução


**Instalações necessárias:**

Node, npm ou yarn e git.

**Instalação do projeto:**
Rode os seguintes comandos no terminal:

`git clone https://gitlab.com/ostickets/front-end.git`

Dentro do diretório do projeto:

`npm install`
ou
`yarn install`

**Execução do projeto:**

`npm start`
ou
`yarn start`

E o projeto deverá rodar em: http://localhost:3000/
