import React from 'react'

import API from 'api/index'
import * as CONSTANTS from 'utils/constants'


import { Modal, Button } from 'react-bootstrap'
import TextField from '@material-ui/core/TextField';
import ViewDayIcon from '@material-ui/icons/ViewDay';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import NoteIcon from '@material-ui/icons/Note';
import AddIcon from '@material-ui/icons/Add';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import 'styles/NewTicket.css'


export default class NewTicket extends React.Component {


    state = {
        ticket_id: false,
        title: "",
        body: "",
        priority: "",
        notes: [],
        state: "",
        owner: "",
        owner_id: false,
        newNote: "",
        gotTicket: false,
        valid_owners: []
    }

    componentDidMount() {
        if (this.props.params) {
            this.setState({
                ticket_id: this.props.params.ticket_id,
                title: this.props.params.title,
                body: this.props.params.body,
                priority: this.props.params.priority,
                notes: this.props.params.notes,
                state: this.props.params.state,
                owner_id: this.props.params.owner_id,
                valid_owners: this.props.params.valid_owners,
            })
        }

    }

    static getDerivedStateFromProps(props, state) {
        if (props && props.show && props.params && !state.gotTicket) {
            return {
                ticket_id: props.params.ticket_id,
                title: props.params.title,
                body: props.params.body,
                priority: props.params.priority,
                notes: props.params.notes,
                state: props.params.state,
                owner_id: props.params.owner_id,
                valid_owners: props.params.valid_owners,
                gotTicket: true,
            }
        }
        return null
    }

    renderNotes() {
        if (this.state.notes) {
            return (
                this.state.notes.map((item, index) => {
                    return (
                        <ListItem key={index} button>
                            <ListItemIcon>
                                <NoteIcon />
                            </ListItemIcon>
                            <ListItemText primary={item.note} secondary={
                                <div>
                                    <p>@{item.username}</p>
                                    <p>{item.date}</p>
                                </div>
                            } />
                        </ListItem>
                    )
                })
            )
        }
    }

    addNewNote() {
        this.state.notes.push({ username: this.props.params.owner_username, note: this.state.newNote, date: new Date().toDateString() })
        this.setState({ notes: this.state.notes, newNote: '' })
        API.post('/tickets/add_note', { token: this.props.token, ticket_id: this.state.ticket_id, note: this.state.newNote }, {
            headers: {
                'Content-Type': 'Application/Json'
            }
        }).then(response => {
            console.log(response)
            if (response.status = 200 && response.data && !response.data.Error) {

            }
            return
        })
    }

    renderSetOwnerButton() {
        if (this.props.params && this.state.valid_owners) {
            return (
                <div style={{ width: '100%', marginBottom: 50 }} >
                    <FormLabel style={{ marginBottom: 25 }} component="legend">Designar responsável:</FormLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        defaultValue={this.state.owner_id}
                        onChange={(event) => this.setState({ owner_id: event.target.value })}
                        style={{ width: '100%' }}
                    >
                        {this.state.valid_owners.map((item, index) => {
                            return (
                                <MenuItem key={index} value={item.user_id}>{item.username}</MenuItem>
                            )
                        })}
                    </Select>
                </div>
            )
        }
    }

    resetComponent() {
        this.setState({
            ticket_id: false,
            title: "",
            body: "",
            priority: "",
            notes: [],
            state: "",
            owner: "",
            newNote: "",
            gotTicket: false,
        })
    }

    render() {
        const { show, handleClose, handleSubmit, params } = this.props
        return (
            <div >
                <Modal onHide={() => false} className="new-ticket" show={show} animation={false}>
                    <Modal.Header className="flex-center-title">
                        <ViewDayIcon style={{ marginRight: 10 }} />
                        <Modal.Title>{params ? 'Edição de Ticket' : 'Novo Ticket'}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body className="ticket-body" >
                        {params &&
                            <div className="ticket-users" >
                                <div>
                                    <p>Criado por:</p>
                                    <div style={{ display: 'flex' }} >
                                        <AccountCircleIcon />
                                        <p>@{params.creator_username}</p>
                                    </div>
                                </div>
                                {params.owner_username &&
                                    <div>
                                        <p>Responsável:</p>
                                        <div style={{ display: 'flex', justifyContent: 'flex-end' }} >
                                            <p>@{params.owner_username}</p>
                                            <AccountCircleIcon />
                                        </div>
                                    </div>
                                }
                            </div>}
                        {this.renderSetOwnerButton()}

                        <TextField
                            onChange={(e) => this.setState({ title: e.target.value })}
                            value={this.state.title}
                            fullWidth
                            type="text"
                            id="ticket-title"
                            label="Título"
                            variant="outlined"
                            style={{ marginBottom: 25 }}
                        />
                        <TextField
                            onChange={(e) => this.setState({ body: e.target.value })}
                            value={this.state.body}
                            rows={4}
                            multiline={true}
                            fullWidth
                            type="text"
                            id="ticket-body"
                            label="Descrição"
                            variant="outlined"
                            style={{ marginBottom: 25 }}
                        />
                        <FormLabel style={{ marginBottom: 25 }} component="legend">Prioridade:</FormLabel>
                        <RadioGroup
                            row
                            aria-label="priority"
                            name="priority"
                            defaultValue={this.state.priority}
                            onChange={(e) => this.setState({ priority: e.target.value })}
                            style={{ marginBottom: 25 }}
                        >
                            <FormControlLabel value="Crítico" control={<Radio style={{ color: 'rgb(100, 22, 163)' }} />} label="Crítico" />
                            <FormControlLabel value="Alto" control={<Radio style={{ color: 'rgb(100, 22, 163)' }} />} label="Alto" />
                            <FormControlLabel value="Médio" control={<Radio style={{ color: 'rgb(100, 22, 163)' }} />} label="Médio" />
                            <FormControlLabel value="Baixo" control={<Radio style={{ color: 'rgb(100, 22, 163)' }} />} label="Baixo" />
                        </RadioGroup>
                        <FormLabel style={{ marginBottom: 25 }} component="legend">Estado:</FormLabel>
                        <RadioGroup
                            row
                            aria-label="state"
                            name="state"
                            defaultValue={this.state.state}
                            onChange={(e) => this.setState({ state: e.target.value })}
                            style={{ marginBottom: 25 }}
                        >
                            <FormControlLabel value="Aberto" control={<Radio style={{ color: 'rgb(100, 22, 163)' }} />} label="Aberto" />
                            <FormControlLabel value="Em Andamento" control={<Radio style={{ color: 'rgb(100, 22, 163)' }} />} label="Em andamento" />
                            <FormControlLabel value="Pendente Cliente" control={<Radio style={{ color: 'rgb(100, 22, 163)' }} />} label="Pendente Cliente" />
                            <FormControlLabel value="Fechado" control={<Radio style={{ color: 'rgb(100, 22, 163)' }} />} label="Fechado" />
                        </RadioGroup>
                        {params &&
                            <div style={{ width: '100%' }} >
                                <FormLabel component="legend" style={{ marginBottom: 25 }} >Notas:</FormLabel>
                                <div className='notes-input' >
                                    <TextField
                                        onChange={(e) => this.setState({ newNote: e.target.value })}
                                        fullWidth
                                        type="text"
                                        id="note-text"
                                        label="Nova anotação"
                                        variant="outlined"
                                    />
                                    <IconButton onClick={() => this.addNewNote()} color="primary" aria-label="add an alarm">
                                        <AddIcon />
                                    </IconButton>
                                </div>
                                <List className='notes-list' >
                                    {this.renderNotes()}
                                </List>
                            </div>}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={() => { this.resetComponent(); handleClose() }}>
                            FECHAR
                        </Button>
                        <Button style={{ background: 'rgb(100, 22, 163)' }} onClick={() => { this.resetComponent(); handleSubmit(this.state) }}>
                            SALVAR
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }

}