import { render, screen } from "@testing-library/react";
import React from "react";
import Tickets from "../Tickets";

describe("TicketCard", () => {
    describe("when show is true", () => {
        it("renders the component", () => {
            render(<Tickets show={true} />);

            expect(screen.getByText("FECHAR")).toBeInTheDocument();
            expect(screen.getByText("SALVAR")).toBeInTheDocument();
        });
    });

    describe("when show is false", () => {
        it("doesn't render the component", () => {
            render(<Tickets show={false} />);

            expect(screen.queryByText("FECHAR")).not.toBeInTheDocument();
            expect(screen.queryByText("SALVAR")).not.toBeInTheDocument();
        });
    });
});
