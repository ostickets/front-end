import { render } from "@testing-library/react";
import React from "react";
import Header from "../Header";

describe("Header", () => {
    describe("when header renders", () => {
        it("must have the email written", () => {
            const { getByText } = render(<Header />);

            expect(getByText("usuario@email.com")).toBeInTheDocument();
        });
    });
});
