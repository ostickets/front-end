import React from 'react'

import UserIcon from '@material-ui/icons/AccountCircle'

import logo from 'assets/images/ost.png'

import 'styles/Header.css'


export default class Header extends React.Component {

    render(){
        return(
            <div className="header-bar" >
                <img alt='logo' className="logo-header" src={logo} />
                <div className="user-header" >
                    <UserIcon  />
                    <h4 style={{marginLeft: 10}}>usuario@email.com</h4>
                </div>
            </div>
        )
    }

}