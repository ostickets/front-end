import React from "react";

import Header from 'components/Header'
import Tickets from 'components/Tickets'

import * as CONSTANTS from 'utils/constants'
import API from 'api/index'
import moment from 'moment'

import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import NoTicketIcon from '@material-ui/icons/LayersClear';
import MyTickets from '@material-ui/icons/ViewCarousel';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { IconButton, Icon } from '@material-ui/core';

import 'styles/Home.css'
import 'primereact/resources/themes/saga-blue/theme.css'
import 'primereact/resources/primereact.min.css'
import 'primeicons/primeicons.css'



export default class Home extends React.Component {

    state = {
        user: false,
        tickets: [
        ],
        showTicket: false,
        ticketChoosed: false,
        user: false,
    }

    async componentDidMount() {
        await this.setState({ user: JSON.parse(localStorage.getItem(CONSTANTS.USER_AUTHENTICATED)) })

        API.post('/user/fetch_tickets', { token: this.state.user.token }, {
            headers: {
                'Content-Type': 'Application/Json'
            }
        }).then(response => {
            if (response.status = 200 && response.data && !response.data.Error) {
                this.setState({ tickets: response.data.tickets })
            }
        })

    }

    chooseTicket(ticket) {
        API.post('/tickets/fetch_ticket', { token: this.state.user.token, ticket_id: ticket.ticket_id }, {
            headers: {
                'Content-Type': 'Application/Json'
            }
        }).then(response => {
            console.log(response)
            if (response.status = 200 && response.data && !response.data.Error) {
                this.setState({ ticketChoosed: { ...ticket, body: response.data.body, notes: response.data.notes, valid_owners: response.data.valid_owners }, showTicket: true })

            }
            return
        })
    }

    renderActionButton(rowData, column) {
        return (
            <>
                <IconButton onClick={() => this.chooseTicket(rowData)} aria-label="edit" color="primary">
                    <Icon>pageview</Icon>
                </IconButton>
            </>
        )
    }

    renderDateTemplate(rowData) {
        return moment(rowData.created_at).format('DD/MM/YYYY h:mm')
    }

    renderTickets() {
        if (this.state.tickets.length > 0) {
            return (
                <div className="tickets" >
                    <DataTable className="tickets-table" value={this.state.tickets} dataKey="id" rowHover
                        paginator rows={4} emptyMessage="Nenhum ticket encontrado" currentPageReportTemplate="Mostrando {first}/{last} de {totalRecords} entradas"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown" rowsPerPageOptions={[10, 25, 50]}>
                        <Column field="title" header="Título" sortable filter filterPlaceholder="Procurar pelo título" />
                        <Column field="created_at" header="Data de abertura" body={this.renderDateTemplate} sortable filter filterPlaceholder="Procurar pela data" />
                        <Column field="state" header="Estado" filter filterPlaceholder="Procurar pelo estado" />
                        <Column field="priority" header="Prioridade" sortable filter filterPlaceholder="Procurar pela prioridade" />
                        <Column field="owner_username" header="Responsável" sortable filter filterPlaceholder="Procurar pelo responsável" />
                        <Column header="Ações" body={(rowData, column) => this.renderActionButton(rowData, column)} headerStyle={{ width: '8em', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible' }} />
                    </DataTable>
                </div>
            )
        }
        return (
            <div className="no-tickets" >
                <NoTicketIcon style={{ fontSize: 100 }} />
                <h3>Você ainda não possuí nenhum ticket</h3>
            </div>
        )

    }

    handleCloseTicket() {
        this.setState({ showTicket: false, ticketChoosed: false })
    }

    handleSubmitTicket(data) {
        if (!this.state.ticketChoosed) {
            API.post('/tickets/create_ticket', { token: this.state.user.token, team_id: 1, ...data }, {
                headers: {
                    'Content-Type': 'Application/Json'
                }
            }).then(response => {
                API.post('/user/fetch_tickets', { token: this.state.user.token }, {
                    headers: {
                        'Content-Type': 'Application/Json'
                    }
                }).then(response => {
                    if (response.status = 200 && response.data && !response.data.Error) {
                        this.setState({ tickets: response.data.tickets, showTicket: false, ticketChoosed: false })
                    }
                })
                if (response.status = 200 && response.data && !response.data.Error) {

                }
                return
            })
        } else {
            API.post('/tickets/set_state', { token: this.state.user.token, ticket_id: data.ticket_id, state: data.state }, {
                headers: {
                    'Content-Type': 'Application/Json'
                }
            }).then(response => {
                if (response.status = 200 && response.data && !response.data.Error) {

                }
                API.post('/tickets/set_priority', { token: this.state.user.token, ticket_id: data.ticket_id, priority: data.priority }, {
                    headers: {
                        'Content-Type': 'Application/Json'
                    }
                }).then(response => {

                    API.post('/user/set_owner', { token: this.state.user.token, ticket_id: data.ticket_id, owner_id: data.owner_id }, {
                        headers: {
                            'Content-Type': 'Application/Json'
                        }
                    }).then(response => {



                        API.post('/user/fetch_tickets', { token: this.state.user.token }, {
                            headers: {
                                'Content-Type': 'Application/Json'
                            }
                        }).then(response => {
                            if (response.status = 200 && response.data && !response.data.Error) {
                                this.setState({ tickets: response.data.tickets, showTicket: false, ticketChoosed: false })
                            }
                        })
                        if (response.status = 200 && response.data && !response.data.Error) {
                        }



                    })


                    if (response.status = 200 && response.data && !response.data.Error) {

                    }
                    return
                })
                return
            })
        }
    }

    render() {
        const { showTicket } = this.state
        return (
            <div className="home-component" >
                <Header />
                <div className="home-body" >
                    <Tickets
                        show={showTicket}
                        handleClose={() => this.handleCloseTicket()}
                        handleSubmit={(data) => this.handleSubmitTicket(data)}
                        params={this.state.ticketChoosed}
                        token={this.state.user.token}
                        user={this.state.user}
                    />
                    <div className="create-ticket" >
                        <Button
                            color="primary"
                            startIcon={<AddIcon />}
                            onClick={() => this.setState({ showTicket: true })}
                        >
                            criar novo ticket
                        </Button>
                    </div>
                    <div className="my-tickets" >
                        <MyTickets fontSize='large' style={{ marginBottom: 5, marginRight: 10 }} />
                        <h2>Meus Tickets:</h2>
                    </div>
                    <div className="tickets-content" >
                        {this.renderTickets()}
                    </div>
                </div>
            </div>
        );
    }
}