import React, { Component } from 'react';

import 'styles/Register.css'


import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import API from 'api/index'
import * as CONSTANTS from 'utils/constants'

export default class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            passwd: '',
            teams_id: '1',
            user_role: 'client',
            token: false
        }
    }

    componentDidMount() {
        this.setState({ token: JSON.parse(localStorage.getItem(CONSTANTS.USER_AUTHENTICATED)).token })
    }

    // Lógica referente a chamada da api de cadastro
    registerUser() {
        API.post('/user/add_user', this.state, {
            headers: {
                'Content-Type': 'Application/Json'
            }
        }).then(response => {
            if (response.status === 200 && response.data && !response.data.Error) {

            }
        })
    }

    render() {
        return (
            <div className="login-component" >
                <div className="login-bg" >
                    <div className="login-content" >
                        <div className="login-card" >
                            <h2>Cadastro</h2>
                            <div className="inputs row" >
                                <TextField onChange={(e) => this.setState({ username: e.target.value })} style={{ marginBottom: 20 }} fullWidth type="username" id="username" label="Username" variant="outlined" size='small' />
                            </div>
                            <div className="inputs row" >
                                <TextField onChange={(e) => this.setState({ passwd: e.target.value })} style={{ marginBottom: 20 }} fullWidth type="password" id="user-password" label="Senha" variant="outlined" size='small' />
                            </div>
                            <div className="inputs row" >
                                <TextField onChange={(e) => this.setState({ user_role: e.target.value })} style={{ marginBottom: 20 }} fullWidth type="role" id="role" label="Perfil" variant="outlined" size='small' />
                            </div>
                            {this.state.user_role === 'agent' ?
                                (
                                    <div className="inputs row" >
                                        <TextField onChange={(e) => this.setState({ teams_id: e.target.value })} style={{ marginBottom: 20 }} fullWidth type="text" id="team" label="Time" variant="outlined" size='small' />
                                    </div>
                                ) : null
                            }

                            <div style={{ marginTop: '2rem' }}>
                                <Button onClick={() => this.registerUser()} variant="contained" color="primary">
                                    Cadastrar
                                </Button>
                            </div>

                            <a href="/" variant="outlined" style={{ marginTop: '1rem' }}>Voltar</a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}