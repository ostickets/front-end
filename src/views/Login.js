import React, { Component } from 'react';

import 'styles/Login.css'

import logo from 'assets/images/ost.png'
import * as CONSTANTS from 'utils/constants'

import { Redirect } from 'react-router';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import API from 'api/index'

import { withRouter } from 'react-router-dom';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            passwd: '',
            user: false
        }
    }

    componentDidMount() {
        if(localStorage.getItem(CONSTANTS.USER_AUTHENTICATED)){
            this.setState({user: true})
        }
    }

    // Lógica referente à autenticação do usuario no sistema.
    loginUser() {
        /*axios.post('http://0.0.0.0:5000/api/v1/login', this.state)
            .then((ret) => {
                console.log(ret);
                this.props.history.push('/home');
            }).catch((error) => {
                console.log(error)
            })*/
        API.post('/login', this.state, {
            headers: {
                'Content-Type': 'Application/Json'
            }
        }).then(response => {
            if (response.status === 200 && response.data && !response.data.Error) {
                localStorage.setItem(CONSTANTS.USER_AUTHENTICATED, JSON.stringify(response.data))
            }
        })
    }


    render() {
        if(this.state.user){
            return <Redirect to='/home' />
        }
        return (
            <div className="login-component" >
                <div className="login-bg" >
                    <div className="login-content" >
                        <div className="login-card" >
                            <div className="card-header" >
                                <img className="ost-logo" alt="logo" width={120} src={logo} />
                            </div>
                            <h2>Login</h2>
                            <div className="inputs row" >
                                <TextField onChange={(e) => this.setState({ username: e.target.value })} style={{ marginBottom: 20 }} fullWidth type="username" id="user-username" label="Username" variant="outlined" />
                            </div>
                            <div className="inputs row" >
                                <TextField onChange={(e) => this.setState({ passwd: e.target.value })} fullWidth type="password" id="user-password" label="Senha" variant="outlined" />
                            </div>
                            <div style={{ marginTop: '2rem' }}>
                                <Button onClick={() => this.loginUser()} variant="contained" color="primary">
                                    ENTRAR
                                </Button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(Login);