import api from 'api'

export const userLogin = (user) =>
    api.post('login', user, {
        headers: {
            'Authorization': `Bearer ${user/* trocar por token */}`,
            'Content-Type': ''
        },
    });