import React from "react";

import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

import * as ROUTES from 'routes/constants'

import Login from 'views/Login'
import Home from 'views/Home'
import Register from 'views/Register'

export default function App() {
    return (
        <Router>
            <Switch>
                <Route exact path={ROUTES.INITIAL_PATH} component={(props) => <Login {...props} />}>
                    <Login />
                </Route>
                <Route path={ROUTES.REGISTER_PATH} component={(props) => <Register {...props} />}>
                    <Register />
                </Route>
                <Route path={ROUTES.HOME_PATH} component={(props) => <Home {...props} />}>
                    <Home />
                </Route>
            </Switch>
        </Router>
    );
}